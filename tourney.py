from pathlib import Path
from collections import Counter

from client import Client
from world_builder import create_battle_royale


def evaluate_five(teams, team_names, out_dir=None):
    assert len(teams) == 5 and len(team_names) == 5
    if out_dir:
        out_dir = Path(out_dir)
        out_dir.mkdir(exist_ok=True)
    total = Counter()

    # Combi 1    
    ordering = [0,1,2,3]
    game_teams = [teams[i] for i in ordering]
    game_names = [team_names[i] for i in ordering]
    points = evaluate_four(game_teams, game_names, out_dir)
    total.update(points)

    # Combi 2
    ordering = [0,1,2,4]
    game_teams = [teams[i] for i in ordering]
    game_names = [team_names[i] for i in ordering]
    points = evaluate_four(game_teams, game_names, out_dir)
    total.update(points)

    # Combi 3    
    ordering = [0,1,3,4]
    game_teams = [teams[i] for i in ordering]
    game_names = [team_names[i] for i in ordering]
    points = evaluate_four(game_teams, game_names, out_dir)
    total.update(points)

    # Combi 4    
    ordering = [0,2,3,4]
    game_teams = [teams[i] for i in ordering]
    game_names = [team_names[i] for i in ordering]
    points = evaluate_four(game_teams, game_names, out_dir)
    total.update(points)

    # Combi 5
    ordering = [1,2,3,4]
    game_teams = [teams[i] for i in ordering]
    game_names = [team_names[i] for i in ordering]
    points = evaluate_four(game_teams, game_names, out_dir)
    total.update(points)

    print(total)
    return total


def evaluate_four(teams, team_names, out_dir=None):
    assert len(teams) == 4 and len(team_names) == 4
    if out_dir:
        out_dir = Path(out_dir)
        out_dir.mkdir(exist_ok=True)
    a,b,c,d = teams
    na, nb, nc, nd = team_names

    total = Counter()
    # game 1
    ordering = [0,1,2,3]
    game_teams = [teams[i] for i in ordering]
    game_names = [team_names[i] for i in ordering]
    game_dir = out_dir/f'game_1  {"  ".join(game_names)}' if out_dir else None
    points = run_game(game_teams, game_names, game_dir)
    total.update(points)

    # game 2
    ordering = [0,3,1,2]
    game_teams = [teams[i] for i in ordering]
    game_names = [team_names[i] for i in ordering]
    game_dir = out_dir/f'game_2  {"  ".join(game_names)}' if out_dir else None
    points = run_game(game_teams, game_names, game_dir)
    total.update(points)

    # game 3
    ordering = [0,2,3,1]
    game_teams = [teams[i] for i in ordering]
    game_names = [team_names[i] for i in ordering]
    game_dir = out_dir/f'game_3  {"  ".join(game_names)}' if out_dir else None
    points = run_game(game_teams, game_names, game_dir)
    total.update(points)

    print(total)
    return total


def run_game(teams, team_names, out_dir=None, max_rounds=50):
    assert len(teams) == 4 and len(team_names) == 4
    a,b,c,d = teams
    na, nb, nc, nd = team_names
    points = Counter({
        na: 0,
        nb: 0,
        nc: 0,
        nd: 0,
    })
    game = create_battle_royale(a,b,c,d,na,nb,nc,nd, max_rounds=50)
    client = Client(game=game)
    client.run(auto_save=False)

    if out_dir:
        client.save(out_dir)

    for player in game.players:
        points[player.name] += player.gathered_points

    print(points)
    return points


if __name__ == '__main__':
    from game.agent_ai import SimpleAI
    teams = [SimpleAI, SimpleAI, SimpleAI, SimpleAI, SimpleAI]
    names = ['Team a', 'team_b' , 'team 3', 'Player 4', 'Player 5ive']
    # points = evaluate_four(teams, names, 'semi_upper')
    points = evaluate_five(teams, names, out_dir='bracket_a')
    import pdb
    pdb.set_trace()
