from __future__ import annotations
import random

from game.commands import Command, AttackCommand, TakeItemCommand, MoveCommand,\
  IdleCommand, UseItemCommand


#######################
# Things that can act #
#######################
class AgentAI:
  @staticmethod
  def decide(state, prev_state=None) -> Command:
    return IdleCommand()


class SimpleAI(AgentAI):
  @staticmethod
  def decide(state, prev_state=None) -> Command:
    for direct_attackable in state['direct_attackables']:
      return AttackCommand(direct_attackable)

    for takable in state['takables']:
      return TakeItemCommand(takable)

    for usable in state['usables']:
      return UseItemCommand(usable)

    random.shuffle(state['movables'])
    for movable in state['movables']:
      return MoveCommand(movable)

    return IdleCommand()


class MonsterSimpleAI(AgentAI):
  @staticmethod
  def decide(state, prev_state=None):
    random.shuffle(state['direct_attackables'])
    for direct_attackable in state['direct_attackables']:
      if 'player_id' in direct_attackable:
        return AttackCommand(direct_attackable)

    #sort indirect_attackables by nearness and then lowest HP for ties
    indirect_attackables = sorted(state['indirect_attackables'],
                                  key=lambda x: (len(x['shortest_path']), x['agent']['health']),
                                  reverse=False)

    for indirect_attackable in indirect_attackables:
      agent = indirect_attackable['agent']
      shortest_path = indirect_attackable['shortest_path']
      if 'player_id' in agent and len(shortest_path) <=3 and random.random() < .66:
        print(f'Chasing {agent["name"]}')
        first_move_dir = shortest_path[0]
        return MoveCommand(first_move_dir)

    random.shuffle(state['movables'])
    for movable in state['movables']:
      return MoveCommand(movable)

    return IdleCommand()


class PlayerOneAI(SimpleAI):
  @staticmethod
  def decide(state, prev_state=None):
    ##################
    # Your Code here #
    ##################

    ##################
    # Your Code here #
    ##################
    return SimpleAI.decide(state)


class PlayerTwoAI(SimpleAI):
  @staticmethod
  def decide(state, prev_state=None):
    return SimpleAI.decide(state)


class PlayerThreeAI(SimpleAI):
  @staticmethod
  def decide(state, prev_state=None):
    return SimpleAI.decide(state)


class PlayerFourAI(SimpleAI):
  @staticmethod
  def decide(state, prev_state=None):
    return SimpleAI.decide(state)


PLAYER_AIS = [PlayerOneAI, PlayerTwoAI, PlayerThreeAI, PlayerFourAI]
