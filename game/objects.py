from __future__ import annotations

from dataclasses import InitVar, dataclass, field
from typing import Type

from func_timeout import func_set_timeout

from game.agent_ai import AgentAI, MonsterSimpleAI, SimpleAI
from game.objects_base import WorldObject, Weapon, UsableItem, PointItem, Agent

##################
# Inventory Item #
##################
@dataclass(eq=False)
class Sword(Weapon):
  name: str = 'sword'
  atk: int = 10
  symbol: str = 's'
  code: str = '04'


@dataclass(eq=False)
class GreatSword(Weapon):
  name: str = 'greatsword'
  atk: int = 20
  symbol: str = 'S'
  code: str = '06'


@dataclass(eq=False)
class Strawberry(PointItem):
  name: str = 'strawberry'
  symbol: str = 'b'
  code: str = '03'
  health_gain: int = 20
  hunger_gain: int = 10
  points: int = 10


@dataclass(eq=False)
class Water(UsableItem):
  name: str = 'water'
  symbol: str = 'w'
  code: str = '02'
  health_gain: int = 30
  hunger_gain: int = 10


##########
# Agents #
# ########
@dataclass(eq=False)
class MonsterM(Agent):
  monster_id: int = field(default=0, repr=False)
  movement: int = 1
  symbol: str = 'M'
  name: str = 'monster'
  base_atk: int = 15
  health: int = 100
  hunger_delta: int = 0
  points: int = 20
  gathered_points: int = 0
  agent_ai: Type[AgentAI] = MonsterSimpleAI

  def __post_init__(self, parent_cell, timeout=None):
    super().__post_init__(parent_cell, timeout=timeout)
    self.code = f'{self.symbol}{self.monster_id}'
    self.name = f'{self.name}_{self.code}'

  @property
  def character_settings(self):
    out = list()
    out.append(f'Start of {self.symbol}')

    out.append(f'Health: {self.health}')
    out.append(f'Hunger: {self.hunger}')

    out.append(f'End')
    return '\n'.join(out)

  @property
  def atk(self):
    return self.base_atk

@dataclass(eq=False)
class BossMonster(MonsterM):
    points: int = 50
    base_atk: int = 20
    symbol: str = 'N'
    name: str = 'boss_monster'


@dataclass(eq=False)
class Player(Agent):
  player_id: int = field(default='0')
  symbol: str = field(default='P')
  movement: int = 1
  base_atk: int = 20
  health: int = 100
  hunger: int = 30
  hunger_delta: int = 1
  max_hunger: int = 50

  points: int = 30
  gathered_points: int = 0
  timeout: InitVar[int] = 1

  def __post_init__(self, parent_cell, timeout=1):
    super().__post_init__(parent_cell, timeout=timeout)
    self.code = f'{self.symbol}{self.player_id}'
    self.symbol = f'{self.player_id}'

    if not self.name:
      self.name = f'player_{self.player_id}'

  @func_set_timeout(1)
  def decide(self, state):
    return super().decide(state)

  @property
  def character_settings(self):
    out = list()
    out.append(f'Start of P')

    out.append(f'maxHealth: {self.max_health}')
    out.append(f'currHealth: {self.health}')
    out.append(f'maxHunger: {self.max_hunger}')
    out.append(f'currHunger: {self.hunger}')
    out.append(f'Score: {self.gathered_points}')

    num_of_item_types = {"02":0,"03":0,"04":0,"06":0}
    for i in self.inventory:
      num_of_item_types[i.code]+=1
    for (k,v) in num_of_item_types.items():
      out.append(f'{k}: {v}')
    out.append(f'Inventory: {", ".join(num_of_item_types.keys())}')
    out.append(f'End')
    return '\n'.join(out)

  @property
  def atk(self):
    bonus_atk = sum([item.atk for item in self.inventory if isinstance(item, Weapon)])
    return self.base_atk + bonus_atk