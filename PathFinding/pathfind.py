# -*- coding: utf-8 -*-
"""
Created on Thu Aug  1 15:59:52 2019

@author: czhengxi
"""

from .search import Problem
from game.commands import Direction

class PathFind(Problem):
    def __init__(self, grid, initial, goal):
        """
        Input:
            grid - 2D boolean array array, where true is empty space and false for obstacle
            
            initial - tuple representing the coordinates of path source
            
            goal - tuple representing the coordinates of path destination
        """
        self.grid = grid
        super().__init__(initial, goal)
    
    def actions(self, state):
        """
        Return the directions where the agent at current state can move to,        
        Note that state is a similar format to initial/goal
        """            
        (y, x) = state
        if y-1 >= 0 and self.grid[y-1][x]:
            yield Direction.UP
        if y+1 < len(self.grid) and self.grid[y+1][x]:
            yield Direction.DOWN
        if x-1 >= 0 and self.grid[y][x-1]:
           yield Direction.LEFT            
        if x+1 < len(self.grid[0]) and self.grid[y][x+1]:
            yield Direction.RIGHT       
    
    def result(self, state, action: Direction):
        """
        Return the new state (position) of the agent after taking action 
        in current state (position).
        """                
        new_state = state
        if action == Direction.UP:
            new_state = (state[0]-1, state[1])
        elif action == Direction.DOWN:
            new_state = (state[0]+1, state[1])
        elif action == Direction.LEFT:
            new_state = (state[0], state[1]-1)
        elif action == Direction.RIGHT:
            new_state = (state[0], state[1]+1)
        return new_state
    
    def h(self, node):
        """
        Return the heuristic value for a given state (agent position).
        Heuristic function used is the sum of number of horizontal and 
        vertical blocks required to move to goal from current position.
        """
        horizontal_dist = abs(node.state[1] - self.goal[1])         
        vertical_dist = abs(node.state[0] - self.goal[0])
        return vertical_dist + horizontal_dist